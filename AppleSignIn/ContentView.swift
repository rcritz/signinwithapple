//
//  ContentView.swift
//  AppleSignIn
//
//  Created by Scott Grosch on 6/7/19.
//

import SwiftUI
import UIKit
import AuthenticationServices

struct ContentView : View {
  @Environment(\.window) var window: UIWindow?
  @State var appleSignInDelegates: AppleSignInDelegates!
  
  var body: some View {
    LoginWithApple()
      .frame(width: 280, height: 60, alignment: .center)
      .tapAction(showAppleLogin)
  }
  
  func createDelegate() {
    appleSignInDelegates = AppleSignInDelegates(window: window)
  }
  
  func showAppleLogin() {
    let appleIDProvider = ASAuthorizationAppleIDProvider()
    let request = appleIDProvider.createRequest()
    request.requestedScopes = [.fullName, .email]
    
    let authorizationController = ASAuthorizationController(authorizationRequests: [request])
    authorizationController.delegate = appleSignInDelegates
    authorizationController.presentationContextProvider = appleSignInDelegates
    authorizationController.performRequests()
  }
}

#if DEBUG
struct ContentView_Previews : PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
#endif
