//
//  AppleSignInDelegates.swift
//  AppleSignIn
//
//  Created by Scott Grosch on 6/7/19.
//

import UIKit
import AuthenticationServices

class AppleSignInDelegates: NSObject {
  weak var window: UIWindow!
  
  init(window: UIWindow?) {
    self.window = window
  }
}


extension AppleSignInDelegates: ASAuthorizationControllerDelegate {
  func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
    print("authorized")
  }
  
  func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    // Handle error.
    print("Not authorized")
  }
}

extension AppleSignInDelegates: ASAuthorizationControllerPresentationContextProviding {
  func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
    return self.window
  }
}
