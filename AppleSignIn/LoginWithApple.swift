//
//  LoginWithApple.swift
//  AppleSignIn
//
//  Created by Scott Grosch on 6/7/19.
//

import SwiftUI
import AuthenticationServices

final class LoginWithApple: UIViewRepresentable {
    func makeUIView(context: Context) -> ASAuthorizationAppleIDButton {
        return ASAuthorizationAppleIDButton()
    }
    
    func updateUIView(_ uiView: ASAuthorizationAppleIDButton, context: Context) {
    }
}

