# SwiftUI & Sign In With Apple

## Add the capability!

Remember to click on your project and add the Sign-In capability, which has already been done in this example.

## Wrap the UIView

The `LoginWithApple` class allows you to add the sign-in button to your UI by returning the UIView you want from the `makeUIView` method.

## Create the class to handle the signing callbacks

*AppleSignInDelegates.swift* contains the callbacks that the sign-in button will use.  You can modify them to handle what you need.  Notice that a `UIWindow` is expected to be passed into the constructor.

## Add the window to the environment.

I spoke to the engineers at the SwiftUI lab and they didn't have a great solution for getting the `UIWindow` to the callbacks, so they suggested I drop it into the environment, just like rcritz did.  The *EnvironmentWindowKey.swift* file handles creating the new key, and ensuring that the `UIWindow` is held as a weak reference.

## Modify the SceneDelegate

In *SceneDelegate.swift* you'll need to store the window into the environment.  You can see how I accomplished that in the `scene(_:willConnectTo:options:)` method.

## Handle taps

FInally, in *ContentView.swift* you can see the code to add the sign-in button to the view, handle the tap, and present things appropriately.  Notice how there's a mutating method to set the delegate.  It's done this way because obviously I want a strong reference to the delegate and we can't add an initializer to a struct that would set this.  You also can't just make the `showAppleLogin` mutating or you can't use it as the tap action.  Good times.
